export default {
  logger: { level: 'debug', transport: ['file', 'console'] },
  middlewares: {
    pre: [
      { ids: 'hrefstart' },
      { __ssdGlobalMiddlewares__: 'sd_7EmxxOk703exD5hF' },
      { __ssdGlobalMiddlewares__: 'cors' }
    ],
    post: [],
    sequences: { IDSAuthroizedAPIs: { pre: [{ ids: 'Authorize' }], post: [] } }
  },
  ids: {
    client_id: 'kgKH_34ozvK8W5Zkk3pUC',
    client_secret:
      'YaFEZp9DD9-G75g4mWxrl8VJSCPndkOt1VpaVUEr2L3Rmo1FSfflHUJK-W9zWmlX2He_Etog7b83HvaIWyH45g',
    issuerURL: 'https://ids.neutrinos.co',
    enabled: true
  }
};
