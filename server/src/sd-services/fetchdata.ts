import { SDBaseService } from '../services/SDBaseService';
import * as httpStatusCodes from 'http-status-codes';
import { Middleware } from '../middleware/Middleware';
import log from '../utils/Logger';
import * as cookieParser from 'cookie-parser';
import { Readable } from 'stream';
import { setInterval } from 'timers';
import * as settings from '../config/config';
import { Issuer, custom } from 'openid-client';
import * as crypto from 'crypto';
import * as url from 'url';

let instance = null;
//CORE_REFERENCE_IMPORTS
//append_imports_start

import { MongoPersistance } from '../utils/ndefault-mongodb/Mongodb/MongoPersistance'; //_splitter_
import * as mongodb from 'mongodb'; //_splitter_
//append_imports_end

export class fetchdata {
  private sdService = new SDBaseService();
  private app;
  private serviceBasePath: string;
  private generatedMiddlewares: Object;
  private serviceName: string;
  private swaggerDocument: Object;
  private globalTimers: any;
  private constructor(
    app,
    generatedeMiddlewares,
    routeCall,
    middlewareCall,
    swaggerDocument,
    globalTimers
  ) {
    this.serviceName = 'fetchdata';
    this.app = app;
    this.serviceBasePath = `${this.app.settings.base}`;
    this.generatedMiddlewares = generatedeMiddlewares;
    this.swaggerDocument = swaggerDocument;
    this.globalTimers = globalTimers;
  }

  static getInstance(
    app?,
    generatedeMiddlewares?,
    routeCall?,
    middlewareCall?,
    swaggerDocument?,
    globalTimers?
  ) {
    if (!instance) {
      instance = new fetchdata(
        app,
        generatedeMiddlewares,
        routeCall,
        middlewareCall,
        swaggerDocument,
        globalTimers
      );
    }
    instance.mountCalls(routeCall, middlewareCall);
    return instance;
  }

  private mountCalls(routeCall, middlewareCall) {
    if (routeCall) {
      this.mountAllPaths();
    }
    if (middlewareCall) {
      this.generatedMiddlewares[this.serviceName] = {};
      this.mountAllMiddlewares();
      this.mountTimers();
    }
  }

  async mountTimers() {
    try {
      //appendnew_flow_fetchdata_TimerStart
    } catch (e) {
      throw e;
    }
  }

  private mountAllMiddlewares() {
    log.debug('mounting all middlewares for service :: fetchdata');

    //appendnew_flow_fetchdata_MiddlewareStart
  }
  private mountAllPaths() {
    log.debug('mounting all paths for service :: fetchdata');

    if (!this.swaggerDocument['paths']['/userInfo']) {
      this.swaggerDocument['paths']['/userInfo'] = {
        get: {
          summary: '',
          description: '',
          consumes: [],
          produces: [],
          parameters: [],
          responses: {}
        }
      };
    } else {
      this.swaggerDocument['paths']['/userInfo']['get'] = {
        summary: '',
        description: '',
        consumes: [],
        produces: [],
        parameters: [],
        responses: {}
      };
    }
    this.app['get'](
      `${this.serviceBasePath}/userInfo`,
      cookieParser(),
      this.sdService.getMiddlesWaresBySequenceId(
        null,
        'pre',
        this.generatedMiddlewares
      ),

      async (req, res, next) => {
        let bh = {};
        try {
          bh = this.sdService.__constructDefault(
            { local: {}, input: {} },
            req,
            res,
            next
          );
          bh = await this.sd_rOFcD5ZaZyyunDg8(bh);
          //appendnew_next_sd_FROBTqkP1AkLXeM2
        } catch (e) {
          return await this.errorHandler(bh, e, 'sd_FROBTqkP1AkLXeM2');
        }
      },
      this.sdService.getMiddlesWaresBySequenceId(
        null,
        'post',
        this.generatedMiddlewares
      )
    );

    if (!this.swaggerDocument['paths']['/diagnosis']) {
      this.swaggerDocument['paths']['/diagnosis'] = {
        get: {
          summary: '',
          description: '',
          consumes: [],
          produces: [],
          parameters: [],
          responses: {}
        }
      };
    } else {
      this.swaggerDocument['paths']['/diagnosis']['get'] = {
        summary: '',
        description: '',
        consumes: [],
        produces: [],
        parameters: [],
        responses: {}
      };
    }
    this.app['get'](
      `${this.serviceBasePath}/diagnosis`,
      cookieParser(),
      this.sdService.getMiddlesWaresBySequenceId(
        null,
        'pre',
        this.generatedMiddlewares
      ),

      async (req, res, next) => {
        let bh = {};
        try {
          bh = this.sdService.__constructDefault(
            { local: {}, input: {} },
            req,
            res,
            next
          );
          bh = await this.sd_qRNvjU13xQqNH4O4(bh);
          //appendnew_next_sd_VRyXof095LboplEx
        } catch (e) {
          return await this.errorHandler(bh, e, 'sd_VRyXof095LboplEx');
        }
      },
      this.sdService.getMiddlesWaresBySequenceId(
        null,
        'post',
        this.generatedMiddlewares
      )
    );

    if (!this.swaggerDocument['paths']['/item']) {
      this.swaggerDocument['paths']['/item'] = {
        get: {
          summary: '',
          description: '',
          consumes: [],
          produces: [],
          parameters: [],
          responses: {}
        }
      };
    } else {
      this.swaggerDocument['paths']['/item']['get'] = {
        summary: '',
        description: '',
        consumes: [],
        produces: [],
        parameters: [],
        responses: {}
      };
    }
    this.app['get'](
      `${this.serviceBasePath}/item`,
      cookieParser(),
      this.sdService.getMiddlesWaresBySequenceId(
        null,
        'pre',
        this.generatedMiddlewares
      ),

      async (req, res, next) => {
        let bh = {};
        try {
          bh = this.sdService.__constructDefault(
            { local: {}, input: {} },
            req,
            res,
            next
          );
          bh = await this.sd_9a7vcDsn2ZfojiVC(bh);
          //appendnew_next_sd_bP5TdOfNMut5wSoc
        } catch (e) {
          return await this.errorHandler(bh, e, 'sd_bP5TdOfNMut5wSoc');
        }
      },
      this.sdService.getMiddlesWaresBySequenceId(
        null,
        'post',
        this.generatedMiddlewares
      )
    );

    if (!this.swaggerDocument['paths']['/doctor']) {
      this.swaggerDocument['paths']['/doctor'] = {
        get: {
          summary: '',
          description: '',
          consumes: [],
          produces: [],
          parameters: [],
          responses: {}
        }
      };
    } else {
      this.swaggerDocument['paths']['/doctor']['get'] = {
        summary: '',
        description: '',
        consumes: [],
        produces: [],
        parameters: [],
        responses: {}
      };
    }
    this.app['get'](
      `${this.serviceBasePath}/doctor`,
      cookieParser(),
      this.sdService.getMiddlesWaresBySequenceId(
        null,
        'pre',
        this.generatedMiddlewares
      ),

      async (req, res, next) => {
        let bh = {};
        try {
          bh = this.sdService.__constructDefault(
            { local: {}, input: {} },
            req,
            res,
            next
          );
          bh = await this.sd_HOwT0GTUcm2fQuBk(bh);
          //appendnew_next_sd_gif6U4a6Rsiiggdw
        } catch (e) {
          return await this.errorHandler(bh, e, 'sd_gif6U4a6Rsiiggdw');
        }
      },
      this.sdService.getMiddlesWaresBySequenceId(
        null,
        'post',
        this.generatedMiddlewares
      )
    );

    if (!this.swaggerDocument['paths']['/procedure']) {
      this.swaggerDocument['paths']['/procedure'] = {
        get: {
          summary: '',
          description: '',
          consumes: [],
          produces: [],
          parameters: [],
          responses: {}
        }
      };
    } else {
      this.swaggerDocument['paths']['/procedure']['get'] = {
        summary: '',
        description: '',
        consumes: [],
        produces: [],
        parameters: [],
        responses: {}
      };
    }
    this.app['get'](
      `${this.serviceBasePath}/procedure`,
      cookieParser(),
      this.sdService.getMiddlesWaresBySequenceId(
        null,
        'pre',
        this.generatedMiddlewares
      ),

      async (req, res, next) => {
        let bh = {};
        try {
          bh = this.sdService.__constructDefault(
            { local: {}, input: {} },
            req,
            res,
            next
          );
          bh = await this.sd_Po9PVVRKFbBIPxWg(bh);
          //appendnew_next_sd_sYjXjJ98E6Og3yeJ
        } catch (e) {
          return await this.errorHandler(bh, e, 'sd_sYjXjJ98E6Og3yeJ');
        }
      },
      this.sdService.getMiddlesWaresBySequenceId(
        null,
        'post',
        this.generatedMiddlewares
      )
    );

    if (!this.swaggerDocument['paths']['/provider']) {
      this.swaggerDocument['paths']['/provider'] = {
        get: {
          summary: '',
          description: '',
          consumes: [],
          produces: [],
          parameters: [],
          responses: {}
        }
      };
    } else {
      this.swaggerDocument['paths']['/provider']['get'] = {
        summary: '',
        description: '',
        consumes: [],
        produces: [],
        parameters: [],
        responses: {}
      };
    }
    this.app['get'](
      `${this.serviceBasePath}/provider`,
      cookieParser(),
      this.sdService.getMiddlesWaresBySequenceId(
        null,
        'pre',
        this.generatedMiddlewares
      ),

      async (req, res, next) => {
        let bh = {};
        try {
          bh = this.sdService.__constructDefault(
            { local: {}, input: {} },
            req,
            res,
            next
          );
          bh = await this.sd_Yq10nPJaQL3wNbVr(bh);
          //appendnew_next_sd_sXZXb4lFyrk6QZrM
        } catch (e) {
          return await this.errorHandler(bh, e, 'sd_sXZXb4lFyrk6QZrM');
        }
      },
      this.sdService.getMiddlesWaresBySequenceId(
        null,
        'post',
        this.generatedMiddlewares
      )
    );
    //appendnew_flow_fetchdata_HttpIn
  }
  //   service flows_fetchdata

  //appendnew_flow_fetchdata_Start

  async sd_rOFcD5ZaZyyunDg8(bh) {
    try {
      bh.input.queryFilter = JSON.parse(bh.input.query.filter || '{}') || {};
      bh.input.options = JSON.parse(bh.input.query.options || '{}') || {};
      console.log(bh.input.query);
      bh = await this.sd_FUgVDzdnrvRgORyI(bh);
      //appendnew_next_sd_rOFcD5ZaZyyunDg8
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_rOFcD5ZaZyyunDg8');
    }
  }

  async sd_FUgVDzdnrvRgORyI(bh) {
    try {
      bh.local.result = await MongoPersistance.getInstance().find(
        'sd_4VPv7azjuP1C06wn',
        'user',
        bh.input.queryFilter,
        bh.input.options
      );
      await this.sd_dWnWuAehSUY2hZdC(bh);
      //appendnew_next_sd_FUgVDzdnrvRgORyI
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_FUgVDzdnrvRgORyI');
    }
  }

  async sd_dWnWuAehSUY2hZdC(bh) {
    try {
      bh.web.res.status(200).send(bh.local.result);

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_dWnWuAehSUY2hZdC');
    }
  }

  async sd_qRNvjU13xQqNH4O4(bh) {
    try {
      bh.input.queryFilter = JSON.parse(bh.input.query.filter || '{}') || {};
      bh.input.options = JSON.parse(bh.input.query.options || '{}') || {};

      bh = await this.sd_TiMh91U8HAoQ6jU8(bh);
      //appendnew_next_sd_qRNvjU13xQqNH4O4
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_qRNvjU13xQqNH4O4');
    }
  }

  async sd_TiMh91U8HAoQ6jU8(bh) {
    try {
      bh.local.result = await MongoPersistance.getInstance().find(
        'sd_4VPv7azjuP1C06wn',
        'Diagnosis',
        bh.input.queryFilter,
        bh.input.options
      );
      await this.sd_wv65BOsvr4Pv1ZTp(bh);
      //appendnew_next_sd_TiMh91U8HAoQ6jU8
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_TiMh91U8HAoQ6jU8');
    }
  }

  async sd_wv65BOsvr4Pv1ZTp(bh) {
    try {
      bh.web.res.status(200).send(bh.local.result);

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_wv65BOsvr4Pv1ZTp');
    }
  }

  async sd_9a7vcDsn2ZfojiVC(bh) {
    try {
      bh.input.queryFilter = JSON.parse(bh.input.query.filter || '{}') || {};
      bh.input.options = JSON.parse(bh.input.query.options || '{}') || {};

      bh = await this.sd_BTroVxmmLzQ41Fhq(bh);
      //appendnew_next_sd_9a7vcDsn2ZfojiVC
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_9a7vcDsn2ZfojiVC');
    }
  }

  async sd_BTroVxmmLzQ41Fhq(bh) {
    try {
      bh.local.result = await MongoPersistance.getInstance().find(
        'sd_4VPv7azjuP1C06wn',
        'Items',
        bh.input.queryFilter,
        bh.input.options
      );
      await this.sd_1ytGfjXlS5NSbiTo(bh);
      //appendnew_next_sd_BTroVxmmLzQ41Fhq
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_BTroVxmmLzQ41Fhq');
    }
  }

  async sd_1ytGfjXlS5NSbiTo(bh) {
    try {
      bh.web.res.status(200).send(bh.local.result);

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_1ytGfjXlS5NSbiTo');
    }
  }

  async sd_HOwT0GTUcm2fQuBk(bh) {
    try {
      bh.input.queryFilter = JSON.parse(bh.input.query.filter || '{}') || {};
      bh.input.options = JSON.parse(bh.input.query.options || '{}') || {};

      bh = await this.sd_GajaIGRLrjaS3NdI(bh);
      //appendnew_next_sd_HOwT0GTUcm2fQuBk
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_HOwT0GTUcm2fQuBk');
    }
  }

  async sd_GajaIGRLrjaS3NdI(bh) {
    try {
      bh.local.result = await MongoPersistance.getInstance().find(
        'sd_4VPv7azjuP1C06wn',
        'Physician',
        bh.input.queryFilter,
        bh.input.options
      );
      await this.sd_Ozw78gTC6ITn1Qyr(bh);
      //appendnew_next_sd_GajaIGRLrjaS3NdI
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_GajaIGRLrjaS3NdI');
    }
  }

  async sd_Ozw78gTC6ITn1Qyr(bh) {
    try {
      bh.web.res.status(200).send(bh.local.result);

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_Ozw78gTC6ITn1Qyr');
    }
  }

  async sd_Po9PVVRKFbBIPxWg(bh) {
    try {
      bh.input.queryFilter = JSON.parse(bh.input.query.filter || '{}') || {};
      bh.input.options = JSON.parse(bh.input.query.options || '{}') || {};

      bh = await this.sd_fHuYyiTV0oAo7FAF(bh);
      //appendnew_next_sd_Po9PVVRKFbBIPxWg
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_Po9PVVRKFbBIPxWg');
    }
  }

  async sd_fHuYyiTV0oAo7FAF(bh) {
    try {
      bh.local.result = await MongoPersistance.getInstance().find(
        'sd_4VPv7azjuP1C06wn',
        'Procedure',
        bh.input.queryFilter,
        bh.input.options
      );
      await this.sd_hR6tMohXFPtTtb3p(bh);
      //appendnew_next_sd_fHuYyiTV0oAo7FAF
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_fHuYyiTV0oAo7FAF');
    }
  }

  async sd_hR6tMohXFPtTtb3p(bh) {
    try {
      bh.web.res.status(200).send(bh.local.result);

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_hR6tMohXFPtTtb3p');
    }
  }

  async sd_Yq10nPJaQL3wNbVr(bh) {
    try {
      bh.input.queryFilter = JSON.parse(bh.input.query.filter || '{}') || {};
      bh.input.options = JSON.parse(bh.input.query.options || '{}') || {};

      bh = await this.sd_O3m6g01WYUwIctnu(bh);
      //appendnew_next_sd_Yq10nPJaQL3wNbVr
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_Yq10nPJaQL3wNbVr');
    }
  }

  async sd_O3m6g01WYUwIctnu(bh) {
    try {
      bh.local.result = await MongoPersistance.getInstance().find(
        'sd_4VPv7azjuP1C06wn',
        'Provider',
        bh.input.queryFilter,
        bh.input.options
      );
      await this.sd_TA0XykoTtjQfieFn(bh);
      //appendnew_next_sd_O3m6g01WYUwIctnu
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_O3m6g01WYUwIctnu');
    }
  }

  async sd_TA0XykoTtjQfieFn(bh) {
    try {
      bh.web.res.status(200).send(bh.local.result);

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_TA0XykoTtjQfieFn');
    }
  }

  //appendnew_node

  async errorHandler(bh, e, src) {
    console.error(e);
    bh.error = e;
    bh.errorSource = src;

    if (
      false
      /*appendnew_next_Catch*/
    ) {
      return bh;
    } else {
      if (bh.web.next) {
        bh.web.next(e);
      } else {
        throw e;
      }
    }
  }
  //appendnew_flow_fetchdata_Catch
}
