import { SDBaseService } from '../services/SDBaseService';
import * as httpStatusCodes from 'http-status-codes';
import { Middleware } from '../middleware/Middleware';
import log from '../utils/Logger';
import * as cookieParser from 'cookie-parser';
import { Readable } from 'stream';
import { setInterval } from 'timers';
import * as settings from '../config/config';
import { Issuer, custom } from 'openid-client';
import * as crypto from 'crypto';
import * as url from 'url';

let instance = null;
//CORE_REFERENCE_IMPORTS
//append_imports_start

import { idsutil } from './idsutil'; //_splitter_
//append_imports_end

export class ids {
  private sdService = new SDBaseService();
  private app;
  private serviceBasePath: string;
  private generatedMiddlewares: Object;
  private serviceName: string;
  private swaggerDocument: Object;
  private globalTimers: any;
  private constructor(
    app,
    generatedeMiddlewares,
    routeCall,
    middlewareCall,
    swaggerDocument,
    globalTimers
  ) {
    this.serviceName = 'ids';
    this.app = app;
    this.serviceBasePath = `${this.app.settings.base}`;
    this.generatedMiddlewares = generatedeMiddlewares;
    this.swaggerDocument = swaggerDocument;
    this.globalTimers = globalTimers;
  }

  static getInstance(
    app?,
    generatedeMiddlewares?,
    routeCall?,
    middlewareCall?,
    swaggerDocument?,
    globalTimers?
  ) {
    if (!instance) {
      instance = new ids(
        app,
        generatedeMiddlewares,
        routeCall,
        middlewareCall,
        swaggerDocument,
        globalTimers
      );
    }
    instance.mountCalls(routeCall, middlewareCall);
    return instance;
  }

  private mountCalls(routeCall, middlewareCall) {
    if (routeCall) {
      this.mountAllPaths();
    }
    if (middlewareCall) {
      this.generatedMiddlewares[this.serviceName] = {};
      this.mountAllMiddlewares();
      this.mountTimers();
    }
  }

  async mountTimers() {
    try {
      //appendnew_flow_ids_TimerStart
    } catch (e) {
      throw e;
    }
  }

  private mountAllMiddlewares() {
    log.debug('mounting all middlewares for service :: ids');

    let mw_hrefstart: Middleware = new Middleware(
      this.serviceName,
      'hrefstart',
      async (req, res, next) => {
        let bh = {};
        try {
          bh = this.sdService.__constructDefault({ local: {} }, req, res, next);
          bh = await this.sd_eGozl8aruzfhSJfq(bh);
          //appendnew_next_sd_R4qOkVLU6kmL1enT
        } catch (e) {
          return await this.errorHandler(bh, e, 'sd_R4qOkVLU6kmL1enT');
        }
      }
    );
    this.generatedMiddlewares[this.serviceName]['hrefstart'] = mw_hrefstart;
    let mw_Authorize: Middleware = new Middleware(
      this.serviceName,
      'Authorize',
      async (req, res, next) => {
        let bh = {};
        try {
          bh = this.sdService.__constructDefault({ local: {} }, req, res, next);
          bh = await this.sd_KwbOT8UOKPEmeVuO(bh);
          //appendnew_next_sd_ClVCTjtyypyfWZpF
        } catch (e) {
          return await this.errorHandler(bh, e, 'sd_ClVCTjtyypyfWZpF');
        }
      }
    );
    this.generatedMiddlewares[this.serviceName]['Authorize'] = mw_Authorize;
    //appendnew_flow_ids_MiddlewareStart
  }
  private mountAllPaths() {
    log.debug('mounting all paths for service :: ids');

    if (!this.swaggerDocument['paths']['/login']) {
      this.swaggerDocument['paths']['/login'] = {
        get: {
          summary: '',
          description: '',
          consumes: [],
          produces: [],
          parameters: [],
          responses: {}
        }
      };
    } else {
      this.swaggerDocument['paths']['/login']['get'] = {
        summary: '',
        description: '',
        consumes: [],
        produces: [],
        parameters: [],
        responses: {}
      };
    }
    this.app['get'](
      `${this.serviceBasePath}/login`,
      cookieParser(),
      this.sdService.getMiddlesWaresBySequenceId(
        null,
        'pre',
        this.generatedMiddlewares
      ),

      async (req, res, next) => {
        let bh = {};
        try {
          bh = this.sdService.__constructDefault(
            { local: {}, input: {} },
            req,
            res,
            next
          );
          bh = await this.sd_Z09ctlDNGXPRhYqh(bh);
          //appendnew_next_sd_kPqszFgO8qwICGtl
        } catch (e) {
          return await this.errorHandler(bh, e, 'sd_kPqszFgO8qwICGtl');
        }
      },
      this.sdService.getMiddlesWaresBySequenceId(
        null,
        'post',
        this.generatedMiddlewares
      )
    );

    if (!this.swaggerDocument['paths']['/login/cb']) {
      this.swaggerDocument['paths']['/login/cb'] = {
        get: {
          summary: '',
          description: '',
          consumes: [],
          produces: [],
          parameters: [],
          responses: {}
        }
      };
    } else {
      this.swaggerDocument['paths']['/login/cb']['get'] = {
        summary: '',
        description: '',
        consumes: [],
        produces: [],
        parameters: [],
        responses: {}
      };
    }
    this.app['get'](
      `${this.serviceBasePath}/login/cb`,
      cookieParser(),
      this.sdService.getMiddlesWaresBySequenceId(
        null,
        'pre',
        this.generatedMiddlewares
      ),

      async (req, res, next) => {
        let bh = {};
        try {
          bh = this.sdService.__constructDefault(
            { local: {}, input: {} },
            req,
            res,
            next
          );
          bh = await this.sd_x2REKyM97HAaGIvM(bh);
          //appendnew_next_sd_GjTr6ofbN4RLEI2o
        } catch (e) {
          return await this.errorHandler(bh, e, 'sd_GjTr6ofbN4RLEI2o');
        }
      },
      this.sdService.getMiddlesWaresBySequenceId(
        null,
        'post',
        this.generatedMiddlewares
      )
    );

    if (!this.swaggerDocument['paths']['/user/info']) {
      this.swaggerDocument['paths']['/user/info'] = {
        get: {
          summary: '',
          description: '',
          consumes: [],
          produces: [],
          parameters: [],
          responses: {}
        }
      };
    } else {
      this.swaggerDocument['paths']['/user/info']['get'] = {
        summary: '',
        description: '',
        consumes: [],
        produces: [],
        parameters: [],
        responses: {}
      };
    }
    this.app['get'](
      `${this.serviceBasePath}/user/info`,
      cookieParser(),
      this.sdService.getMiddlesWaresBySequenceId(
        'IDSAuthroizedAPIs',
        'pre',
        this.generatedMiddlewares
      ),

      async (req, res, next) => {
        let bh = {};
        try {
          bh = this.sdService.__constructDefault(
            { local: {}, input: {} },
            req,
            res,
            next
          );
          bh = await this.sd_VbTbNZgePC8OHvIA(bh);
          //appendnew_next_sd_9FrBkLyZOwgjR0s9
        } catch (e) {
          return await this.errorHandler(bh, e, 'sd_9FrBkLyZOwgjR0s9');
        }
      },
      this.sdService.getMiddlesWaresBySequenceId(
        'IDSAuthroizedAPIs',
        'post',
        this.generatedMiddlewares
      )
    );

    if (!this.swaggerDocument['paths']['/logout']) {
      this.swaggerDocument['paths']['/logout'] = {
        get: {
          summary: '',
          description: '',
          consumes: [],
          produces: [],
          parameters: [],
          responses: {}
        }
      };
    } else {
      this.swaggerDocument['paths']['/logout']['get'] = {
        summary: '',
        description: '',
        consumes: [],
        produces: [],
        parameters: [],
        responses: {}
      };
    }
    this.app['get'](
      `${this.serviceBasePath}/logout`,
      cookieParser(),
      this.sdService.getMiddlesWaresBySequenceId(
        null,
        'pre',
        this.generatedMiddlewares
      ),

      async (req, res, next) => {
        let bh = {};
        try {
          bh = this.sdService.__constructDefault(
            { local: {}, input: {} },
            req,
            res,
            next
          );
          bh = await this.sd_Uihe6j1Nnah4s8Js(bh);
          //appendnew_next_sd_BWNcBWvHUFcPtSm6
        } catch (e) {
          return await this.errorHandler(bh, e, 'sd_BWNcBWvHUFcPtSm6');
        }
      },
      this.sdService.getMiddlesWaresBySequenceId(
        null,
        'post',
        this.generatedMiddlewares
      )
    );

    if (!this.swaggerDocument['paths']['/logout/cb']) {
      this.swaggerDocument['paths']['/logout/cb'] = {
        get: {
          summary: '',
          description: '',
          consumes: [],
          produces: [],
          parameters: [],
          responses: {}
        }
      };
    } else {
      this.swaggerDocument['paths']['/logout/cb']['get'] = {
        summary: '',
        description: '',
        consumes: [],
        produces: [],
        parameters: [],
        responses: {}
      };
    }
    this.app['get'](
      `${this.serviceBasePath}/logout/cb`,
      cookieParser(),
      this.sdService.getMiddlesWaresBySequenceId(
        null,
        'pre',
        this.generatedMiddlewares
      ),

      async (req, res, next) => {
        let bh = {};
        try {
          bh = this.sdService.__constructDefault(
            { local: {}, input: {} },
            req,
            res,
            next
          );
          bh = await this.sd_bWSAEMVsifBFMOI0(bh);
          //appendnew_next_sd_p4M3l5jrMGwtHUhV
        } catch (e) {
          return await this.errorHandler(bh, e, 'sd_p4M3l5jrMGwtHUhV');
        }
      },
      this.sdService.getMiddlesWaresBySequenceId(
        null,
        'post',
        this.generatedMiddlewares
      )
    );
    //appendnew_flow_ids_HttpIn
  }
  //   service flows_ids

  //appendnew_flow_ids_Start

  async sd_Z09ctlDNGXPRhYqh(bh) {
    try {
      bh.local.idsConfigured = false;
      if (
        settings.default.hasOwnProperty('ids') &&
        settings.default['ids'].hasOwnProperty('client_id') &&
        settings.default['ids'].hasOwnProperty('client_secret')
      ) {
        bh.local.idsConfigured = true;
      }
      bh = await this.sd_Uhx3xrkF4fkAgCjY(bh);
      //appendnew_next_sd_Z09ctlDNGXPRhYqh
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_Z09ctlDNGXPRhYqh');
    }
  }

  async sd_Uhx3xrkF4fkAgCjY(bh) {
    try {
      let otherwiseFlag = true;
      if (
        this.sdService.operators['true'](
          bh.local.idsConfigured,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_FQ23ftKgvWzpXrc7(bh);
        otherwiseFlag = false;
      }
      if (
        this.sdService.operators['else'](
          otherwiseFlag,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_0OZ0stIPtca4UH6A(bh);
        otherwiseFlag = false;
      }

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_Uhx3xrkF4fkAgCjY');
    }
  }

  async sd_FQ23ftKgvWzpXrc7(bh) {
    try {
      bh.local.reqParams = {
        state: crypto.randomBytes(16).toString('hex'),
        nonce: crypto.randomBytes(16).toString('hex'),
        isMobile: bh.input.query.isMobile,
        redirectTo: bh.input.query.redirectTo
      };
      bh = await this.sd_qZoWfl5X9X1RyALn(bh);
      //appendnew_next_sd_FQ23ftKgvWzpXrc7
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_FQ23ftKgvWzpXrc7');
    }
  }

  async sd_qZoWfl5X9X1RyALn(bh) {
    try {
      let requestObject = bh.web.req;
      if (requestObject.session) {
        requestObject.session.data = bh.local.reqParams;
      }
      bh = await this.sd_sBKJrpvjTA1HDVsP(bh);
      //appendnew_next_sd_qZoWfl5X9X1RyALn
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_qZoWfl5X9X1RyALn');
    }
  }

  async sd_sBKJrpvjTA1HDVsP(bh) {
    try {
      const idsutilInstance: idsutil = idsutil.getInstance();
      let outputVariables = await idsutilInstance.getIDSClientInstance(null);
      bh.input.client = outputVariables.input.clientInstance;

      bh = await this.sd_p9Os2kWdzTV0v09c(bh);
      //appendnew_next_sd_sBKJrpvjTA1HDVsP
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_sBKJrpvjTA1HDVsP');
    }
  }

  async sd_p9Os2kWdzTV0v09c(bh) {
    try {
      const idsutilInstance: idsutil = idsutil.getInstance();
      let outputVariables = await idsutilInstance.getAuthorizationParams(null);
      bh.input.authParams = outputVariables.input.authParams;

      bh = await this.sd_YTwYCFIEQ4Hoswa8(bh);
      //appendnew_next_sd_p9Os2kWdzTV0v09c
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_p9Os2kWdzTV0v09c');
    }
  }

  async sd_YTwYCFIEQ4Hoswa8(bh) {
    try {
      const authorizationRequest = Object.assign(
        {
          redirect_uri: url.resolve(bh.web.req.href, '/api/login/cb'),
          scope: 'openid profile email address phone user',
          state: bh.local.reqParams.state,
          nonce: bh.local.reqParams.nonce,
          response_type: bh.input.client.response_types[0]
        },
        bh.input.authParams
      );

      bh.local.redirectHeaders = {
        location: bh.input.client.authorizationUrl(authorizationRequest)
      };

      await this.sd_SDhmM8ZwIkgFnKBo(bh);
      //appendnew_next_sd_YTwYCFIEQ4Hoswa8
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_YTwYCFIEQ4Hoswa8');
    }
  }

  async sd_SDhmM8ZwIkgFnKBo(bh) {
    try {
      bh.web.res.set(bh.local.redirectHeaders);

      bh.web.res.status(302).send('redirecting');

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_SDhmM8ZwIkgFnKBo');
    }
  }

  async sd_0OZ0stIPtca4UH6A(bh) {
    try {
      bh.local.res = {
        message:
          'IDS client not registered. Register on the Neutrinos Stuido and try again'
      };
      await this.sd_Hh50s1n3CHlZs3RW(bh);
      //appendnew_next_sd_0OZ0stIPtca4UH6A
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_0OZ0stIPtca4UH6A');
    }
  }

  async sd_Hh50s1n3CHlZs3RW(bh) {
    try {
      bh.web.res.status(404).send(bh.local.res.message);

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_Hh50s1n3CHlZs3RW');
    }
  }

  async sd_eGozl8aruzfhSJfq(bh) {
    try {
      const protocol =
        bh.input.headers['x-forwarded-proto'] || bh.web.req.protocol;
      const href =
        protocol + '://' + bh.web.req.get('Host') + bh.web.req.originalUrl;
      bh.web.req.href = href;
      await this.sd_2O4l8jtiHAqgrj0z(bh);
      //appendnew_next_sd_eGozl8aruzfhSJfq
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_eGozl8aruzfhSJfq');
    }
  }

  async sd_2O4l8jtiHAqgrj0z(bh) {
    try {
      bh.web.next();
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_2O4l8jtiHAqgrj0z');
    }
  }

  async sd_x2REKyM97HAaGIvM(bh) {
    try {
      let requestObject = bh.web.req;
      if (requestObject.session) {
        bh.input.sessionParams = JSON.parse(
          JSON.stringify(requestObject.session)
        );
      }

      bh = await this.sd_5yGkcy8cqdPw164K(bh);
      //appendnew_next_sd_x2REKyM97HAaGIvM
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_x2REKyM97HAaGIvM');
    }
  }

  async sd_5yGkcy8cqdPw164K(bh) {
    try {
      const idsutilInstance: idsutil = idsutil.getInstance();
      let outputVariables = await idsutilInstance.getIDSClientInstance(null);
      bh.input.client = outputVariables.input.clientInstance;

      bh = await this.sd_bvmMdEY9O5l2HKVg(bh);
      //appendnew_next_sd_5yGkcy8cqdPw164K
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_5yGkcy8cqdPw164K');
    }
  }

  async sd_bvmMdEY9O5l2HKVg(bh) {
    try {
      const params = bh.input.client.callbackParams(bh.web.req);
      let tokenset = await bh.input.client.callback(
        url.resolve(bh.web.req.href, 'cb'),
        params,
        {
          nonce: bh.input.sessionParams.data.nonce,
          state: bh.input.sessionParams.data.state
        }
      );

      bh.local.redirectTo = bh.input.sessionParams.data.redirectTo;

      bh.local.userDetails = {
        tokenset: Object.assign({}, tokenset),
        userInfo: await bh.input.client.userinfo(tokenset['access_token'])
      };
      bh.local.userDetails['tokenset']['claims'] = Object.assign(
        {},
        tokenset.claims()
      );
      bh = await this.sd_OtwYgi7kKKumkyWJ(bh);
      //appendnew_next_sd_bvmMdEY9O5l2HKVg
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_bvmMdEY9O5l2HKVg');
    }
  }

  async sd_OtwYgi7kKKumkyWJ(bh) {
    try {
      let requestObject = bh.web.req;
      if (requestObject.session) {
        requestObject.session.data = bh.local.userDetails;
      }
      bh = await this.sd_WDZBBKSuWqs4MaEs(bh);
      //appendnew_next_sd_OtwYgi7kKKumkyWJ
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_OtwYgi7kKKumkyWJ');
    }
  }

  async sd_WDZBBKSuWqs4MaEs(bh) {
    try {
      let otherwiseFlag = true;
      if (
        this.sdService.operators['se'](
          bh.input.sessionParams.data.isMobile,
          'true',
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_y7ikpqaEOPZApcVm(bh);
        otherwiseFlag = false;
      }
      if (
        this.sdService.operators['else'](
          otherwiseFlag,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_lVaHutKVxSr52Jwj(bh);
        otherwiseFlag = false;
      }

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_WDZBBKSuWqs4MaEs');
    }
  }

  async sd_y7ikpqaEOPZApcVm(bh) {
    try {
      bh.local.htmlResponse = `
 <html>
   <script>
      let _timer;
      _timer = setInterval(() => {
                  if(window.webkit) {
                      window.webkit.messageHandlers.cordova_iab.postMessage(JSON.stringify({'auth': 'success'}));
                      clearInterval(_timer);
                  }
              }, 250);
      
   </script>
</html>`;
      await this.sd_HGX07mLqW0UGlH06(bh);
      //appendnew_next_sd_y7ikpqaEOPZApcVm
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_y7ikpqaEOPZApcVm');
    }
  }

  async sd_HGX07mLqW0UGlH06(bh) {
    try {
      bh.web.res.status(200).send(bh.local.htmlResponse);

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_HGX07mLqW0UGlH06');
    }
  }

  async sd_lVaHutKVxSr52Jwj(bh) {
    try {
      bh.local.redirectHeaders = {
        location: bh.local.redirectTo
      };
      await this.sd_6P8LCl26e1oQpLfN(bh);
      //appendnew_next_sd_lVaHutKVxSr52Jwj
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_lVaHutKVxSr52Jwj');
    }
  }

  async sd_6P8LCl26e1oQpLfN(bh) {
    try {
      bh.web.res.set(bh.local.redirectHeaders);

      bh.web.res.status(302).send('Redirecting');

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_6P8LCl26e1oQpLfN');
    }
  }

  async sd_VbTbNZgePC8OHvIA(bh) {
    try {
      let requestObject = bh.web.req;
      if (requestObject.session) {
        bh.local.session = JSON.parse(JSON.stringify(requestObject.session));
      }

      await this.sd_onSqOcUzbMor8tn4(bh);
      //appendnew_next_sd_VbTbNZgePC8OHvIA
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_VbTbNZgePC8OHvIA');
    }
  }

  async sd_onSqOcUzbMor8tn4(bh) {
    try {
      bh.web.res.status(200).send(bh.local.session.data.userInfo);

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_onSqOcUzbMor8tn4');
    }
  }

  async sd_gMNCu7lBPlnmvtXr(bh) {
    try {
      bh.web.res.redirect('/api/login');
      //appendnew_next_sd_gMNCu7lBPlnmvtXr
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_gMNCu7lBPlnmvtXr');
    }
  }

  async sd_Uihe6j1Nnah4s8Js(bh) {
    try {
      let requestObject = bh.web.req;
      if (requestObject.session) {
        bh.local.sessionData = JSON.parse(
          JSON.stringify(requestObject.session)
        );
      }

      bh = await this.sd_tLWq8ll2vCO0wTSL(bh);
      //appendnew_next_sd_Uihe6j1Nnah4s8Js
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_Uihe6j1Nnah4s8Js');
    }
  }

  async sd_tLWq8ll2vCO0wTSL(bh) {
    try {
      bh.local.sessionExists = false;
      if (
        bh.local.sessionData &&
        bh.local.sessionData.data &&
        bh.local.sessionData.data.tokenset
      ) {
        bh.local.sessionData['data']['redirectTo'] =
          bh.input.query['redirectTo'];
        bh.local.sessionData['data']['isMobile'] = bh.input.query['isMobile'];
        bh.local.sessionExists = true;
      } else {
        delete bh.local.sessionData['redirectTo'];
      }
      bh = await this.sd_8SOITuNytwCna98r(bh);
      //appendnew_next_sd_tLWq8ll2vCO0wTSL
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_tLWq8ll2vCO0wTSL');
    }
  }

  async sd_8SOITuNytwCna98r(bh) {
    try {
      let requestObject = bh.web.req;
      if (requestObject.session) {
        requestObject.session.data = bh.local.sessionData.data;
      }
      bh = await this.sd_z8msKcZiTQNHHFkv(bh);
      //appendnew_next_sd_8SOITuNytwCna98r
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_8SOITuNytwCna98r');
    }
  }

  async sd_z8msKcZiTQNHHFkv(bh) {
    try {
      const idsutilInstance: idsutil = idsutil.getInstance();
      let outputVariables = await idsutilInstance.getIDSClientInstance(null);
      bh.input.client = outputVariables.input.clientInstance;

      bh = await this.sd_xCEjIReYDVlLv5f9(bh);
      //appendnew_next_sd_z8msKcZiTQNHHFkv
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_z8msKcZiTQNHHFkv');
    }
  }

  async sd_xCEjIReYDVlLv5f9(bh) {
    try {
      let otherwiseFlag = true;
      if (
        this.sdService.operators['true'](
          bh.local.sessionExists,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_uG0OvYu53ILYZpq0(bh);
        otherwiseFlag = false;
      }
      if (
        this.sdService.operators['else'](
          otherwiseFlag,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_IYZODXT7aLgnSYiX(bh);
        otherwiseFlag = false;
      }

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_xCEjIReYDVlLv5f9');
    }
  }

  async sd_uG0OvYu53ILYZpq0(bh) {
    try {
      await Promise.all([
        bh.local.sessionData.data.tokenset.access_token
          ? bh.input.client.revoke(
              bh.local.sessionData.data.tokenset.access_token,
              'access_token'
            )
          : undefined,
        bh.local.sessionData.data.tokenset.refresh_token
          ? bh.input.client.revoke(
              bh.local.sessionData.data.tokenset.refresh_token,
              'refresh_token'
            )
          : undefined
      ]);

      bh.local.res = {
        idsURL: url.format(
          Object.assign(
            url.parse(bh.input.client.issuer.end_session_endpoint),
            {
              search: null,
              query: {
                id_token_hint: bh.local.sessionData.data.tokenset.id_token,
                post_logout_redirect_uri: url.resolve(
                  bh.web.req.href,
                  '/api/logout/cb'
                ),
                client_id: settings.default['ids']['client_id']
              }
            }
          )
        ),
        sessionExists: true
      };
      await this.sd_HBVc9bDbixlLzX3v(bh);
      //appendnew_next_sd_uG0OvYu53ILYZpq0
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_uG0OvYu53ILYZpq0');
    }
  }

  async sd_HBVc9bDbixlLzX3v(bh) {
    try {
      bh.web.res.status(200).send(bh.local.res);

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_HBVc9bDbixlLzX3v');
    }
  }

  async sd_IYZODXT7aLgnSYiX(bh) {
    try {
      bh.local.res = {
        sessionExists: false
      };
      await this.sd_HBVc9bDbixlLzX3v(bh);
      //appendnew_next_sd_IYZODXT7aLgnSYiX
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_IYZODXT7aLgnSYiX');
    }
  }

  async sd_bWSAEMVsifBFMOI0(bh) {
    try {
      let requestObject = bh.web.req;
      if (requestObject.session) {
        bh.local.sessionData = JSON.parse(
          JSON.stringify(requestObject.session)
        );
      }

      bh = await this.sd_ID6NcysjoOJcYdI2(bh);
      //appendnew_next_sd_bWSAEMVsifBFMOI0
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_bWSAEMVsifBFMOI0');
    }
  }

  async sd_ID6NcysjoOJcYdI2(bh) {
    try {
      let requestObject = bh.web.req;
      if (requestObject.session) {
        let p = function() {
          return new Promise((resolve, reject) => {
            requestObject.session.destroy(function(error) {
              if (error) {
                return reject(error);
              }
              return resolve();
            });
          });
        };
        await p();
      }
      bh = await this.sd_S0GCDB2e1iAzOoQQ(bh);
      //appendnew_next_sd_ID6NcysjoOJcYdI2
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_ID6NcysjoOJcYdI2');
    }
  }

  async sd_S0GCDB2e1iAzOoQQ(bh) {
    try {
      let otherwiseFlag = true;
      if (
        this.sdService.operators['se'](
          bh.local.sessionData.data.isMobile,
          'true',
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_qCaoy0pvDSUgEbUu(bh);
        otherwiseFlag = false;
      }
      if (
        this.sdService.operators['else'](
          otherwiseFlag,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_jSVhOI9O1gqm9Q1u(bh);
        otherwiseFlag = false;
      }

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_S0GCDB2e1iAzOoQQ');
    }
  }

  async sd_qCaoy0pvDSUgEbUu(bh) {
    try {
      bh.local.res = `<html>
   <script>
      var _timer;
      _timer = setInterval(() => {
                  if(window.webkit) {
                      window.webkit.messageHandlers.cordova_iab.postMessage(JSON.stringify({'auth': 'success'}));
                      clearInterval(_timer);
                  }
              }, 250);
      
   </script>
</html>`;
      await this.sd_bi2hvY8jAWiJx6hU(bh);
      //appendnew_next_sd_qCaoy0pvDSUgEbUu
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_qCaoy0pvDSUgEbUu');
    }
  }

  async sd_bi2hvY8jAWiJx6hU(bh) {
    try {
      bh.web.res.status(200).send(bh.local.res);

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_bi2hvY8jAWiJx6hU');
    }
  }

  async sd_jSVhOI9O1gqm9Q1u(bh) {
    try {
      bh.local.redirectHeaders = {
        location: bh.local.sessionData.data.redirectTo
      };
      await this.sd_riTval1iWMHTMoAN(bh);
      //appendnew_next_sd_jSVhOI9O1gqm9Q1u
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_jSVhOI9O1gqm9Q1u');
    }
  }

  async sd_riTval1iWMHTMoAN(bh) {
    try {
      bh.web.res.set(bh.local.redirectHeaders);

      bh.web.res.status(302).send('redirecting');

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_riTval1iWMHTMoAN');
    }
  }

  async sd_KwbOT8UOKPEmeVuO(bh) {
    try {
      bh.local = {};
      bh = await this.sd_PFzzaPfJePeSCLbu(bh);
      //appendnew_next_sd_KwbOT8UOKPEmeVuO
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_KwbOT8UOKPEmeVuO');
    }
  }

  async sd_PFzzaPfJePeSCLbu(bh) {
    try {
      let requestObject = bh.web.req;
      if (requestObject.session) {
        bh.local.sessionData = JSON.parse(
          JSON.stringify(requestObject.session)
        );
      }

      bh = await this.sd_JFn4uUL627QrKOFr(bh);
      //appendnew_next_sd_PFzzaPfJePeSCLbu
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_PFzzaPfJePeSCLbu');
    }
  }

  async sd_JFn4uUL627QrKOFr(bh) {
    try {
      bh.local.sessionExists = false;

      if (
        bh.local.sessionData &&
        bh.local.sessionData.data &&
        bh.local.sessionData.data.tokenset &&
        bh.local.sessionData.data.tokenset.access_token &&
        bh.local.sessionData.data.tokenset.refresh_token
      ) {
        bh.local.sessionExists = true;
      }
      bh = await this.sd_zcCoERN7bVRaM4Gs(bh);
      //appendnew_next_sd_JFn4uUL627QrKOFr
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_JFn4uUL627QrKOFr');
    }
  }

  async sd_zcCoERN7bVRaM4Gs(bh) {
    try {
      let otherwiseFlag = true;
      if (
        this.sdService.operators['true'](
          bh.local.sessionExists,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_6lYu9DpQMgieKF0U(bh);
        otherwiseFlag = false;
      }
      if (
        this.sdService.operators['else'](
          otherwiseFlag,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_2xtNiUmEbFHTBpoS(bh);
        otherwiseFlag = false;
      }

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_zcCoERN7bVRaM4Gs');
    }
  }

  async sd_6lYu9DpQMgieKF0U(bh) {
    try {
      const idsutilInstance: idsutil = idsutil.getInstance();
      let outputVariables = await idsutilInstance.handleTokenExpiry(
        bh.local.sessionData,
        null
      );
      bh.local.newSession = outputVariables.input.newSession;

      bh = await this.sd_R8ziaAQwwKwtqy2l(bh);
      //appendnew_next_sd_6lYu9DpQMgieKF0U
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_6lYu9DpQMgieKF0U');
    }
  }

  async sd_R8ziaAQwwKwtqy2l(bh) {
    try {
      let otherwiseFlag = true;
      if (
        this.sdService.operators['false'](
          bh.local.newSession,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_k8or4mHX1tKIz825(bh);
        otherwiseFlag = false;
      }
      if (
        this.sdService.operators['else'](
          otherwiseFlag,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_K7P4HGyNbNbH5eOn(bh);
        otherwiseFlag = false;
      }

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_R8ziaAQwwKwtqy2l');
    }
  }

  async sd_k8or4mHX1tKIz825(bh) {
    try {
      let requestObject = bh.web.req;
      if (requestObject.session) {
        let p = function() {
          return new Promise((resolve, reject) => {
            requestObject.session.destroy(function(error) {
              if (error) {
                return reject(error);
              }
              return resolve();
            });
          });
        };
        await p();
      }
      bh = await this.sd_rLpbIWCXe4fTnIHA(bh);
      //appendnew_next_sd_k8or4mHX1tKIz825
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_k8or4mHX1tKIz825');
    }
  }

  async sd_rLpbIWCXe4fTnIHA(bh) {
    try {
      bh.local.res = {
        code: 'TOKEN_EXPIRED',
        message: 'Token invalid or access revoked'
      };
      await this.sd_o7LxGPhgSFTlviSi(bh);
      //appendnew_next_sd_rLpbIWCXe4fTnIHA
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_rLpbIWCXe4fTnIHA');
    }
  }

  async sd_o7LxGPhgSFTlviSi(bh) {
    try {
      bh.web.res.status(403).send(bh.local.res);

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_o7LxGPhgSFTlviSi');
    }
  }

  async sd_K7P4HGyNbNbH5eOn(bh) {
    try {
      let otherwiseFlag = true;
      if (
        this.sdService.operators['true'](
          bh.local.newSession.rotated,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_GTY2vajOGPgaJdIK(bh);
        otherwiseFlag = false;
      }
      if (
        this.sdService.operators['else'](
          otherwiseFlag,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_mmT1jpEzDA8TqQxr(bh);
        otherwiseFlag = false;
      }

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_K7P4HGyNbNbH5eOn');
    }
  }

  async sd_GTY2vajOGPgaJdIK(bh) {
    try {
      delete bh.local.newSession.rotated;
      bh = await this.sd_3M0TpHWC9UyuXOl8(bh);
      //appendnew_next_sd_GTY2vajOGPgaJdIK
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_GTY2vajOGPgaJdIK');
    }
  }

  async sd_3M0TpHWC9UyuXOl8(bh) {
    try {
      let requestObject = bh.web.req;
      if (requestObject.session) {
        requestObject.session.data = bh.local.newSession;
      }
      await this.sd_mmT1jpEzDA8TqQxr(bh);
      //appendnew_next_sd_3M0TpHWC9UyuXOl8
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_3M0TpHWC9UyuXOl8');
    }
  }

  async sd_mmT1jpEzDA8TqQxr(bh) {
    try {
      bh.web.next();
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_mmT1jpEzDA8TqQxr');
    }
  }

  async sd_2xtNiUmEbFHTBpoS(bh) {
    try {
      let otherwiseFlag = true;
      if (
        this.sdService.operators['cont'](
          bh.input.path,
          '/user/info',
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_GN0tJpd9t8h9BuKc(bh);
        otherwiseFlag = false;
      }
      if (
        this.sdService.operators['else'](
          otherwiseFlag,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_D3pwpsyZdrRkkCbK(bh);
        otherwiseFlag = false;
      }

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_2xtNiUmEbFHTBpoS');
    }
  }

  async sd_GN0tJpd9t8h9BuKc(bh) {
    try {
      bh.local.res = { message: 'Session expired' };
      await this.sd_o7LxGPhgSFTlviSi(bh);
      //appendnew_next_sd_GN0tJpd9t8h9BuKc
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_GN0tJpd9t8h9BuKc');
    }
  }

  async sd_D3pwpsyZdrRkkCbK(bh) {
    try {
      bh.local.res = { code: 'NO_SESSION', message: 'Session not present' };
      await this.sd_o7LxGPhgSFTlviSi(bh);
      //appendnew_next_sd_D3pwpsyZdrRkkCbK
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_D3pwpsyZdrRkkCbK');
    }
  }

  //appendnew_node

  async errorHandler(bh, e, src) {
    console.error(e);
    bh.error = e;
    bh.errorSource = src;

    if (
      false ||
      (await this.sd_4NPaLrDxYXvtSWDZ(bh)) ||
      (await this.sd_VPvMWyI7DHngcaiC(bh))
      /*appendnew_next_Catch*/
    ) {
      return bh;
    } else {
      if (bh.web.next) {
        bh.web.next(e);
      } else {
        throw e;
      }
    }
  }
  async sd_4NPaLrDxYXvtSWDZ(bh) {
    const nodes = [
      'sd_p9Os2kWdzTV0v09c',
      'sd_GjTr6ofbN4RLEI2o',
      'sd_5yGkcy8cqdPw164K',
      'sd_bvmMdEY9O5l2HKVg',
      'sd_x2REKyM97HAaGIvM',
      'sd_WDZBBKSuWqs4MaEs',
      'sd_y7ikpqaEOPZApcVm',
      'sd_lVaHutKVxSr52Jwj',
      'sd_HGX07mLqW0UGlH06',
      'sd_6P8LCl26e1oQpLfN'
    ];
    if (nodes.includes(bh.errorSource)) {
      bh = await this.sd_gMNCu7lBPlnmvtXr(bh);
      //appendnew_next_sd_4NPaLrDxYXvtSWDZ
      return true;
    }
    return false;
  }
  async sd_VPvMWyI7DHngcaiC(bh) {
    const nodes = ['sd_6lYu9DpQMgieKF0U'];
    if (nodes.includes(bh.errorSource)) {
      bh = await this.sd_rLpbIWCXe4fTnIHA(bh);
      //appendnew_next_sd_VPvMWyI7DHngcaiC
      return true;
    }
    return false;
  }
  //appendnew_flow_ids_Catch
}
