import { SDBaseService } from '../services/SDBaseService';
import * as httpStatusCodes from 'http-status-codes';
import { Middleware } from '../middleware/Middleware';
import log from '../utils/Logger';
import * as cookieParser from 'cookie-parser';
import { Readable } from 'stream';
import { setInterval } from 'timers';
import * as settings from '../config/config';
import { Issuer, custom } from 'openid-client';
import * as crypto from 'crypto';
import * as url from 'url';

let instance = null;
//CORE_REFERENCE_IMPORTS
//append_imports_start

//append_imports_end

export class idsutil {
  private sdService = new SDBaseService();
  private app;
  private serviceBasePath: string;
  private generatedMiddlewares: Object;
  private serviceName: string;
  private swaggerDocument: Object;
  private globalTimers: any;
  private constructor(
    app,
    generatedeMiddlewares,
    routeCall,
    middlewareCall,
    swaggerDocument,
    globalTimers
  ) {
    this.serviceName = 'idsutil';
    this.app = app;
    this.serviceBasePath = `${this.app.settings.base}`;
    this.generatedMiddlewares = generatedeMiddlewares;
    this.swaggerDocument = swaggerDocument;
    this.globalTimers = globalTimers;
  }

  static getInstance(
    app?,
    generatedeMiddlewares?,
    routeCall?,
    middlewareCall?,
    swaggerDocument?,
    globalTimers?
  ) {
    if (!instance) {
      instance = new idsutil(
        app,
        generatedeMiddlewares,
        routeCall,
        middlewareCall,
        swaggerDocument,
        globalTimers
      );
    }
    instance.mountCalls(routeCall, middlewareCall);
    return instance;
  }

  private mountCalls(routeCall, middlewareCall) {
    if (routeCall) {
      this.mountAllPaths();
    }
    if (middlewareCall) {
      this.generatedMiddlewares[this.serviceName] = {};
      this.mountAllMiddlewares();
      this.mountTimers();
    }
  }

  async mountTimers() {
    try {
      //appendnew_flow_idsutil_TimerStart
    } catch (e) {
      throw e;
    }
  }

  private mountAllMiddlewares() {
    log.debug('mounting all middlewares for service :: idsutil');

    //appendnew_flow_idsutil_MiddlewareStart
  }
  private mountAllPaths() {
    log.debug('mounting all paths for service :: idsutil');
    //appendnew_flow_idsutil_HttpIn
  }
  //   service flows_idsutil

  async getIDSClientInstance(clientInstance = null, ...others) {
    try {
      var bh = {
        input: {
          clientInstance: clientInstance
        },
        local: {}
      };
      bh = this.sdService.__constructDefault(bh);
      bh = await this.sd_CJCR9JKAJXhMZ2FF(bh);
      //appendnew_next_getIDSClientInstance
      return (
        // formatting output variables
        {
          input: {
            clientInstance: bh.input.clientInstance
          },
          local: {}
        }
      );
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_Tuy1INOehLau5bdd');
    }
  }

  async getAuthorizationParams(authParams = null, ...others) {
    try {
      var bh = {
        input: {
          authParams: authParams
        },
        local: {}
      };
      bh = this.sdService.__constructDefault(bh);
      bh = await this.sd_Vwd3HvV0MSHjUJ2d(bh);
      //appendnew_next_getAuthorizationParams
      return (
        // formatting output variables
        {
          input: {
            authParams: bh.input.authParams
          },
          local: {}
        }
      );
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_YqxXLAFs4m5jiL40');
    }
  }

  async handleTokenExpiry(existingSession = '', newSession = '', ...others) {
    try {
      var bh = {
        input: {
          existingSession: existingSession,
          newSession: newSession
        },
        local: {}
      };
      bh = this.sdService.__constructDefault(bh);
      bh = await this.sd_wnTTM0fTs4iaFWgt(bh);
      //appendnew_next_handleTokenExpiry
      return (
        // formatting output variables
        {
          input: {
            newSession: bh.input.newSession
          },
          local: {}
        }
      );
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_UykCT6HCvoPWwROL');
    }
  }

  //appendnew_flow_idsutil_Start

  //__server_service_designer_class_variable_declaration__client
  client: any;
  async sd_CJCR9JKAJXhMZ2FF(bh) {
    try {
      bh.local.client = this.client;
      bh = await this.sd_yU8FfFzo0iAlPErm(bh);
      //appendnew_next_sd_CJCR9JKAJXhMZ2FF
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_CJCR9JKAJXhMZ2FF');
    }
  }

  async sd_yU8FfFzo0iAlPErm(bh) {
    try {
      let otherwiseFlag = true;
      if (
        this.sdService.operators['istype'](
          bh.local.client,
          'undefined',
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_m4IIIPQUrokZhvwK(bh);
        otherwiseFlag = false;
      }
      if (
        this.sdService.operators['else'](
          otherwiseFlag,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_YJ8ztEsJ5li0HnVN(bh);
        otherwiseFlag = false;
      }

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_yU8FfFzo0iAlPErm');
    }
  }

  async sd_m4IIIPQUrokZhvwK(bh) {
    try {
      const DEFAULT_HTTP_OPTIONS = {
        timeout: 60000
      };

      custom.setHttpOptionsDefaults({
        timeout: DEFAULT_HTTP_OPTIONS.timeout
      });
      log.info(
        `Identity server default HTTP options : ${DEFAULT_HTTP_OPTIONS}`
      );
      const issuer = await Issuer.discover(
        settings.default['ids']['issuerURL']
      );
      log.info(`Identity server discovered at : ${issuer.issuer}`);
      const client = await new issuer.Client({
        client_id: settings.default['ids']['client_id'],
        client_secret: settings.default['ids']['client_secret']
      });
      client[custom.clock_tolerance] = process.env.CLOCK_TOLERANCE;
      log.info('Client connected...');
      bh.input.clientInstance = client;
      bh = await this.sd_OBRk6xDeMv0SbOyn(bh);
      //appendnew_next_sd_m4IIIPQUrokZhvwK
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_m4IIIPQUrokZhvwK');
    }
  }

  async sd_OBRk6xDeMv0SbOyn(bh) {
    try {
      this.client = bh.input.clientInstance;
      //appendnew_next_sd_OBRk6xDeMv0SbOyn
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_OBRk6xDeMv0SbOyn');
    }
  }

  async sd_YJ8ztEsJ5li0HnVN(bh) {
    try {
      bh.input.clientInstance = this.client;
      //appendnew_next_sd_YJ8ztEsJ5li0HnVN
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_YJ8ztEsJ5li0HnVN');
    }
  }

  async sd_Vwd3HvV0MSHjUJ2d(bh) {
    try {
      bh.input.authParams = {
        scope: 'openid profile email address phone offline_access user',
        prompt: 'consent'
      };
      //appendnew_next_sd_Vwd3HvV0MSHjUJ2d
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_Vwd3HvV0MSHjUJ2d');
    }
  }

  async sd_wnTTM0fTs4iaFWgt(bh) {
    try {
      const tokenset = bh.input.existingSession.data.tokenset;
      bh.local.expires_at = tokenset['expires_at'] * 1000;
      bh.local.refreshTime = new Date().valueOf() + 300000; // 5min before
      bh = await this.sd_8wd5muZrVdvb94T9(bh);
      //appendnew_next_sd_wnTTM0fTs4iaFWgt
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_wnTTM0fTs4iaFWgt');
    }
  }

  async sd_8wd5muZrVdvb94T9(bh) {
    try {
      let otherwiseFlag = true;
      if (
        this.sdService.operators['gt'](
          bh.local.expires_at,
          bh.local.refreshTime,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_ciNIW0oNEbpMu3bM(bh);
        otherwiseFlag = false;
      }
      if (
        this.sdService.operators['else'](
          otherwiseFlag,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_9d4VNniZd1L6d9CV(bh);
        otherwiseFlag = false;
      }

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_8wd5muZrVdvb94T9');
    }
  }

  async sd_ciNIW0oNEbpMu3bM(bh) {
    try {
      bh.input.newSession = bh.input.existingSession.data;
      //appendnew_next_sd_ciNIW0oNEbpMu3bM
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_ciNIW0oNEbpMu3bM');
    }
  }

  async sd_9d4VNniZd1L6d9CV(bh) {
    try {
      const idsutilInstance: idsutil = idsutil.getInstance();
      let outputVariables = await idsutilInstance.getIDSClientInstance(null);
      bh.input.client = outputVariables.input.clientInstance;

      bh = await this.sd_Er1yntRYTsa04MAP(bh);
      //appendnew_next_sd_9d4VNniZd1L6d9CV
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_9d4VNniZd1L6d9CV');
    }
  }

  async sd_Er1yntRYTsa04MAP(bh) {
    try {
      bh.local.refresh_token = await bh.input.client.introspect(
        bh.input.existingSession.data.tokenset.refresh_token,
        'refresh_token'
      );
      bh = await this.sd_0cgyBxrIXLpevDwG(bh);
      //appendnew_next_sd_Er1yntRYTsa04MAP
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_Er1yntRYTsa04MAP');
    }
  }

  async sd_0cgyBxrIXLpevDwG(bh) {
    try {
      let otherwiseFlag = true;
      if (
        this.sdService.operators['true'](
          bh.local.refresh_token.active,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_6kn5mktUVBpHAtvT(bh);
        otherwiseFlag = false;
      }
      if (
        this.sdService.operators['else'](
          otherwiseFlag,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_qg2rdLVqoo3utK7I(bh);
        otherwiseFlag = false;
      }

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_0cgyBxrIXLpevDwG');
    }
  }

  async sd_6kn5mktUVBpHAtvT(bh) {
    try {
      bh.input.newSession = { rotated: true };
      bh.input.newSession['tokenset'] = await bh.input.client.refresh(
        bh.input.existingSession.data.tokenset.refresh_token
      );
      bh.input.newSession['userInfo'] = await bh.input.client.userinfo(
        bh.input.newSession['tokenset']['access_token']
      );
      bh.input.newSession['tokenset']['claims'] = Object.assign(
        {},
        bh.input.newSession['tokenset'].claims()
      );
      //appendnew_next_sd_6kn5mktUVBpHAtvT
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_6kn5mktUVBpHAtvT');
    }
  }

  async sd_qg2rdLVqoo3utK7I(bh) {
    try {
      bh.input.newSession = false;
      //appendnew_next_sd_qg2rdLVqoo3utK7I
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_qg2rdLVqoo3utK7I');
    }
  }

  //appendnew_node

  async errorHandler(bh, e, src) {
    console.error(e);
    bh.error = e;
    bh.errorSource = src;

    if (
      false ||
      (await this.sd_AzvIUl5G0788BWiK(bh))
      /*appendnew_next_Catch*/
    ) {
      return bh;
    } else {
      if (bh.web.next) {
        bh.web.next(e);
      } else {
        throw e;
      }
    }
  }
  async sd_AzvIUl5G0788BWiK(bh) {
    const nodes = ['handleTokenExpiry'];
    if (nodes.includes(bh.errorSource)) {
      bh = await this.sd_qg2rdLVqoo3utK7I(bh);
      //appendnew_next_sd_AzvIUl5G0788BWiK
      return true;
    }
    return false;
  }
  //appendnew_flow_idsutil_Catch
}
