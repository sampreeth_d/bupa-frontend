//CORE_REFERENCE_IMPORTS
//CORE_REFERENCE_IMPORT-rulesflow
import { rulesflow } from './sd-services/rulesflow';
//CORE_REFERENCE_IMPORT-fetchdata
import { fetchdata } from './sd-services/fetchdata';
//CORE_REFERENCE_IMPORT-idsutil
import { idsutil } from './sd-services/idsutil';
//CORE_REFERENCE_IMPORT-ids
import { ids } from './sd-services/ids';

export const UserRoutes = [
//CORE_REFERENCE_PUSH_TO_ROUTE_ARRAY
  //CORE_REFERENCE_PUSH_TO_ROUTE_ARRAY-rulesflow
rulesflow,
  //CORE_REFERENCE_PUSH_TO_ROUTE_ARRAY-fetchdata
fetchdata,
  //CORE_REFERENCE_PUSH_TO_ROUTE_ARRAY-idsutil
idsutil,
  //CORE_REFERENCE_PUSH_TO_ROUTE_ARRAY-ids
ids,
];