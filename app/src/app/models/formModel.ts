
class Policies {
    public POLICY: string;
    public HOLDERCHECK: Boolean;
    constructor() {
        this.POLICY = '';
        this.HOLDERCHECK = false;
    }
}

class Benefits {
    public BENEFITS: string;
    constructor() {
        this.BENEFITS = '';
    }
}

class Documents {
    public DOCARR: Array<any>;
    constructor() {
        this.DOCARR = [];
    }
}


class Claims {
    public DIAGNOSIS: Array<any>;
    public PROCEDURE: Array<any>;
    public CONSULTAIONSTARTDATE: Number;
    public CONSULTAIONENDDATE: Number;
    public DOCTOR: string;
    public SYMPTOM: string;
    public SYMPTOMDATE: Number;
    public TREATMENT: Array<any>;
    public OTHERINSURANCE: string;
    public SETTLED: string;
    constructor() {
        this.DIAGNOSIS = [];
        this.PROCEDURE = [];
        this.CONSULTAIONSTARTDATE = null;
        this.CONSULTAIONENDDATE = null;
        this.DOCTOR = '';
        this.SYMPTOM = '';
        this.SYMPTOMDATE = null;
        this.TREATMENT = [];
        this.OTHERINSURANCE = 'No';
        this.SETTLED = 'No';

    }
}

export class FormData {
    public POLICIES: Policies;
    public BENEFITS: Benefits;
    public DOCUMENTS: Documents;
    public CLAIM: Claims;
    constructor() {
        this.POLICIES = new Policies();
        this.BENEFITS = new Benefits();
        this.DOCUMENTS = new Documents();
        this.CLAIM = new Claims();
    }
}


