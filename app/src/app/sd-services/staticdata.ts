/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE CLASS NAME*/
import { Injectable } from '@angular/core';
//CORE_REFERENCE_IMPORTS
import { SDBaseService } from '../../app/n-services/SDBaseService';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router, NavigationEnd, NavigationStart } from '@angular/router';
//append_imports_start

//append_imports_end

declare const window: any;
declare const cordova: any;

@Injectable()
export class staticdata {
  constructor(
    private sdService: SDBaseService,
    private router: Router,
    private matSnackBar: MatSnackBar
  ) {}

  //   service flows_staticdata

  async genericGet(key: any = undefined, ...others) {
    try {
      var bh = {
        input: {
          key: key
        },
        local: {
          Result: undefined
        }
      };
      bh = this.sdService.__constructDefault(bh);
      bh = await this.sd_vO2GHWivFY7YiN70(bh);
      //appendnew_next_genericGet
      return (
        // formatting output variables
        {
          input: {},
          local: {
            Result: bh.local.Result
          }
        }
      );
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_d5KMmsdMcmW380Mx');
    }
  }

  //appendnew_flow_staticdata_Start

  async sd_vO2GHWivFY7YiN70(bh) {
    try {
      bh.url = `${bh.system.environment.properties.SSD_URL}${bh.input.key}`;

      console.log('url', bh.url);
      bh = await this.sd_ol3y8Awd7mCIUKYO(bh);
      //appendnew_next_sd_vO2GHWivFY7YiN70
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_vO2GHWivFY7YiN70');
    }
  }

  async sd_ol3y8Awd7mCIUKYO(bh) {
    try {
      let requestOptions = {
        url: bh.url,
        method: 'get',
        responseType: 'json',
        reportProgress: undefined,
        headers: {},
        params: {},
        body: undefined
      };
      bh.local.Result = await this.sdService.nHttpRequest(requestOptions);
      //appendnew_next_sd_ol3y8Awd7mCIUKYO
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_ol3y8Awd7mCIUKYO');
    }
  }

  //appendnew_node

  async errorHandler(bh, e, src) {
    console.error(e);
    bh.error = e;
    bh.errorSource = src;

    if (
      false
      /*appendnew_next_Catch*/
    ) {
      return bh;
    } else {
      throw e;
    }
  }
  //appendnew_flow_staticdata_Catch
}
