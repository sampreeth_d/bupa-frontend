/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE CLASS NAME*/
import { Injectable } from '@angular/core';
//CORE_REFERENCE_IMPORTS
import { SDBaseService } from '../../app/n-services/SDBaseService';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router, NavigationEnd, NavigationStart } from '@angular/router';
//append_imports_start

//append_imports_end

declare const window: any;
declare const cordova: any;

@Injectable()
export class rules {
  constructor(
    private sdService: SDBaseService,
    private router: Router,
    private matSnackBar: MatSnackBar
  ) {}

  //   service flows_rules

  async rules(reqBody: any = undefined, ...others) {
    try {
      var bh = {
        input: {
          reqBody: reqBody
        },
        local: {
          result: undefined
        }
      };
      bh = this.sdService.__constructDefault(bh);
      bh = await this.sd_7nACt2z4aAzP5ajH(bh);
      //appendnew_next_rules
      return (
        // formatting output variables
        {
          input: {},
          local: {
            result: bh.local.result
          }
        }
      );
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_jltxmzocMloYTH9a');
    }
  }

  //appendnew_flow_rules_Start

  async sd_7nACt2z4aAzP5ajH(bh) {
    try {
      bh.url = `http://35.208.139.128/data/validateRisk/`;

      console.log(bh);
      bh = await this.sd_KKey7CRltU8xravy(bh);
      //appendnew_next_sd_7nACt2z4aAzP5ajH
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_7nACt2z4aAzP5ajH');
    }
  }

  async sd_KKey7CRltU8xravy(bh) {
    try {
      let requestOptions = {
        url: bh.url,
        method: 'post',
        responseType: 'json',
        reportProgress: undefined,
        headers: {
          Authorization:
            'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJmaXJzdE5hbWUiOiJvcmczNCIsImxhc3ROYW1lIjoiQWRtaW4iLCJ1c2VybmFtZSI6Im9yZzM0YWRtaW4iLCJ1c2VyS2V5Ijoib3JnMzRhZG1pbiIsImRpc3BsYXlOYW1lIjoib3JnMzQgQWRtaW4iLCJncm91cExpc3QiOlsicHVibGljX3VzZXIiLCJwcml2YXRlX3VzZXIiLCJPUkdfQURNSU4iLCJORVVUUklOT1NfVVNFUiJdLCJvcmdhbmlzYXRpb25JZCI6Im9yZzM0IiwiZGJEYXRhSW1wb3J0RXhwb3J0UGVybWlzc2lvbiI6dHJ1ZSwiaWF0IjoxNjEyNjE1NzM4LCJleHAiOjE2MTI2MjI5Mzh9.RU6TdvvZCLGWQJqmLZPVAwcj-ih8GQ2loIIRLA-5YHQ'
        },
        params: {},
        body: bh.input.reqBody
      };
      bh.local.result = await this.sdService.nHttpRequest(requestOptions);
      //appendnew_next_sd_KKey7CRltU8xravy
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_KKey7CRltU8xravy');
    }
  }

  //appendnew_node

  async errorHandler(bh, e, src) {
    console.error(e);
    bh.error = e;
    bh.errorSource = src;

    if (
      false
      /*appendnew_next_Catch*/
    ) {
      return bh;
    } else {
      throw e;
    }
  }
  //appendnew_flow_rules_Catch
}
