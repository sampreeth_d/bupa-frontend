import { Component } from "@angular/core";
import { NLocalStorageService, NTokenService } from "neutrinos-seed-services";
import { Router } from '@angular/router';
import { documentserviceService } from '../services/documentservice/documentservice.service';
@Component({
  selector: "app-root",
  template: ` <div
    fxLayout="column wrap"
    style="background:whitesmoke;"
    fxLayoutAlign="start stretch"
    [fxShow]="true"
    [fxHide]="false"
  >
    <div
      fxLayout="row wrap"
      style="background-color:#0079c7"
      fxFlex="100"
      fxLayoutAlign="start stretch"
      [fxShow]="true"
      [fxHide]="false"
    >
      <div
        fxLayout="row wrap"
        fxFlex="30"
        fxLayoutAlign="start stretch"
        [fxShow]="true"
        [fxHide]="false"
      >
        <img imgSrc="/Web/logo.png" alt="Bupa-logo" />
      </div>
      <div
        fxLayout="row wrap"
        fxFlex="68"
        fxLayoutAlign="end center"
        [fxShow]="true"
        [fxHide]="false"
      >
      <mat-icon matTooltip="DashBoard" style="color:white;cursor:pointer; padding-right:2rem" (click) ="routeToDash()">home</mat-icon>
        <mat-icon matTooltip="logout" style="color:white;cursor:pointer">logout</mat-icon>
      </div>
    </div>
    <router-outlet></router-outlet>
    <n-snackbar></n-snackbar>`,
})
export class LayoutComponent {
  constructor(
    private nLocalstorage: NLocalStorageService,
    private nTokenService: NTokenService,
    private router: Router,
    private common: documentserviceService
  ) { }
  ngOnInit() {
    if (this.nLocalstorage.getValue("accessToken")) {
      this.nTokenService.updateSessionStorage();
    }
    this.common.getAllStaticData();
  }
  routeToDash() {
    this.router.navigate(['/dashboard']);
  }
}
