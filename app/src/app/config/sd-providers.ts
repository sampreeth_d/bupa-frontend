import { SDBaseService } from 'app/n-services/SDBaseService';
//CORE_REFERENCE_IMPORTS
//CORE_REFERENCE_IMPORT-staticdata
import { staticdata } from '../sd-services/staticdata';
//CORE_REFERENCE_IMPORT-rules
import { rules } from '../sd-services/rules';

export const sdProviders = [
  SDBaseService,
  //CORE_REFERENCE_PUSH_TO_SD_ARRAY
  //CORE_REFERENCE_PUSH_TO_SD_ARRAY-staticdata
  staticdata,
  //CORE_REFERENCE_PUSH_TO_SD_ARRAY-rules
  rules
];
