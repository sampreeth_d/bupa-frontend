/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE CLASS NAME*/
import { Injectable, OnInit } from '@angular/core';
import { datamodelService } from '../../services/datamodel/datamodel.service';
import { staticdata } from '../../sd-services/staticdata';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class documentserviceService implements OnInit {

    documentArr = [];
    apisArr = ['diagnosis', 'item', 'doctor', 'procedure', 'provider'];
    apiObj = {};
    apiData: BehaviorSubject<Object> = new BehaviorSubject<Object>({});
    constructor(private dataModelService: datamodelService,
        private staticData: staticdata) {

    }
    ngOnInit() {
        this.getAllStaticData();
        this.documentArr = this.dataModelService.formDataInfo.DOCUMENTS.DOCARR;
    }
    handleFileChange(event, documentName) {
        const documents = event['target']['files'][0];
        const documentObj = {};
        // const docName = documents['name'];
        documentObj['name'] = documentName;
        const reader: FileReader = new FileReader();
        reader.readAsDataURL(documents);
        reader.onloadend = () => {
            documentObj['content'] = reader.result;
        };
        this.documentArr.unshift(documentObj);
        event.target.value = '';
        this.dataModelService.formDataInfo.DOCUMENTS.DOCARR = this.documentArr;
        return this.documentArr;
    }
    public removeDocument(index) {
        this.documentArr.splice(index, 1);
        this.dataModelService.formDataInfo.DOCUMENTS.DOCARR = this.documentArr;
        return this.documentArr;
    }

    public getDocumentArr() {
        return this.documentArr;
    }

    public getAllStaticData() {
        let promiseArr = [];
        for (let i = 0; i < this.apisArr.length; i++) {
            promiseArr.push(new Promise((resolve, reject) => {
                this.staticData.genericGet(this.apisArr[i]).then((res) => {
                    if (res['local']['Result'].length) {
                        this.apiObj[this.apisArr[i]] = res.local.Result || [];
                        this.apiData.next(this.apiObj);
                        return resolve(res.local.Result);
                    }
                }).catch((err) => {
                    console.log(err);
                    return reject(err);
                });
            }));
        }

        Promise.all(promiseArr).then((result) => {
            console.log('All results', this.apiObj);
            return this.apiObj;

        });
    }
}
