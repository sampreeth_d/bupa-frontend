/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE CLASS NAME*/
import { Injectable } from '@angular/core';
import {FormData} from '../../models/formModel';

@Injectable()
export class datamodelService {
    formDataInfo : FormData;

    constructor() {
        this.getFormData();
    }

    getFormData() {
        this.formDataInfo = new FormData();
        return this.formDataInfo;
    }
}
