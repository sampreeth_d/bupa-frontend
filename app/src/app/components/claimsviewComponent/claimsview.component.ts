/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE SELECTOR TEMPLATE_URL AND CLASS NAME*/
import { Component, OnInit } from '@angular/core'
import { NBaseComponent } from '../../../../../app/baseClasses/nBase.component';
import { Router } from '@angular/router';

@Component({
    selector: 'bh-claimsview',
    templateUrl: './claimsview.template.html'
})



export class claimsviewComponent extends NBaseComponent implements OnInit {

    datas = [{
        voucherNo: 'XXXXXXX',
        status: 'Done'
    },
    {
        voucherNo: 'XXXXXXX',
        status: 'Pending for CIR reply'
    }
    ];
    voucher = false;
    voucherData ;

    appealClaim = 'No'
    constructor(private router: Router) {
        super();
    }

    ngOnInit() {

    }

    VoucherDetails(data) {
       this.voucher = true;
       this.voucherData = data;
    }
}

