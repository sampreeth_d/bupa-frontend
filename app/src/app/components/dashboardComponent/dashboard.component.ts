/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE SELECTOR TEMPLATE_URL AND CLASS NAME*/
import { Component, OnInit } from '@angular/core'
import { NBaseComponent } from '../../../../../app/baseClasses/nBase.component';
import { FormBuilder, FormGroup, Validators, FormControl, FormArray } from '@angular/forms';
import { Router } from '@angular/router';
import { documentserviceService } from '../../services/documentservice/documentservice.service';
import { datamodelService } from '../../services/datamodel/datamodel.service';
import { rules } from '../../sd-services/rules';

@Component({
    selector: 'bh-dashboard',
    templateUrl: './dashboard.template.html'
})

export class dashboardComponent extends NBaseComponent implements OnInit {

    firstFormGroup: FormGroup;
    secondFormGroup: FormGroup;
    thirdFormGroup: FormGroup;
    isOptional = false;
    memberName = 'John Doe';
    item;
    amount;
    procedures;
    viewClaim = false;

    steps = [
        { stepno: 1, id: 'policy', disp: 'Eligible Policies', width: '20' },
        { stepno: 2, id: 'risk', disp: 'Benefits', width: '20' },
        { stepno: 3, id: 'additional', disp: 'Record View', width: '20' },
        { stepno: 4, id: 'quotesummary', disp: 'Voucher Details', width: '20' },
        { stepno: 5, id: 'underwriting', disp: 'Claims Submission', width: '20' },
    ];


    policies = [
        { name: ' Corporate HealthNet Plus', value: 'CHP' },
        { name: ' Company Care', value: 'CC' }
    ]
    benefits = [
        { name: 'Outpatient', value: 'Outpatient' },
        { name: 'In Patient', value: 'In Patient' }
    ]

    diagnosis = [];

    procedure = [];
    provider = [];

    doctors = [];

    treatments = [{
        Description: "Diag. Imaging & Lab Tests",
        Code: "C054 Total"
    },
    {
        Description: "Specialist",
        Code: "C052 Total"
    },
    {
        Description: "General Practitioner",
        Code: "C050 Total"
    }]

    symptoms = [
        "Chest Pain",
        "Hematochezia",
        "Headache",
        "Infection, persistent swelling, erythema",
        "rectal bleeding"
    ];
    documentArr = [];
    documentName = 'document';
    dataModel;
    data;

    constructor(private _formBuilder: FormBuilder,
        private router: Router,
        public document: documentserviceService,
        private dataModelService: datamodelService,
        private rulesService: rules) {
        super();
        this.document.apiData.subscribe((res) => {
            console.log(res);
            this.diagnosis = res['diagnosis'] || [];
            this.procedure = res['procedure'] || [];
            this.provider = res['provider'] || [];
            this.doctors = res['doctor'] || [];
            return res;
        })

    }

    ngOnInit() {
        this.firstFormGroup = this._formBuilder.group({
            policySelected: new FormControl('', Validators.required),
            policyHolderCheck: new FormControl(false, Validators.required)

        });
        this.secondFormGroup = this._formBuilder.group({
            benefits: new FormControl('', Validators.required)
        });

        this.thirdFormGroup = this._formBuilder.group({
            diagnosis: new FormControl([], Validators.required),
            procedure: new FormControl([], Validators.required),
            consultationFromDate: new FormControl('', Validators.required),
            consultationToDate: new FormControl('', Validators.required),
            provider: new FormControl('', Validators.required),
            doctor: new FormControl('', Validators.required),
            symptom: new FormControl([]),
            dateOfSymptom: new FormControl(''),
            treatment: new FormControl([], Validators.required),
            totalAmount: new FormControl(''),
            otherInsurance: new FormControl('No', Validators.required),
            attributes: this._formBuilder.array([]),
            settled: new FormControl('No', Validators.required)
        });
        this.dataModel = this.dataModelService.formDataInfo;
        this.documentArr = this.dataModel['DOCUMENTS']['DOCARR'];

        console.log('this', this.dataModel);

    }

    intializeTreatment() {
        const control = <FormArray>this.thirdFormGroup.controls['attributes'];
        control['controls'] = []
        for (let i = 0; i < this.thirdFormGroup.value.treatment.length; i++) {
            control.push(this._createTreatmentForm())
        }
        console.log('as', this.thirdFormGroup)
    }

    _createTreatmentForm() {
        return this._formBuilder.group({
            item: new FormControl('', Validators.required),
            procedure: new FormControl('', Validators.required),
            amount: new FormControl('', Validators.required)
        })
    }

    policyNext() {
        const req = {
            "productId": "BupaClaim",
            "facts": {
                "startDate": 1549443219,
                "endDate": 1644137619,
                "ConsultationDate": 1612601619
            },
            "productVersion": "1.0"
        }
        this.rulesService.rules(req).then((res) => {
            console.log(res);
        })
        this.dataModel['POLICIES']['POLICY'] = this.firstFormGroup.value.policySelected;
        this.dataModel['POLICIES']['HOLDERCHECK'] = this.firstFormGroup.value.policyHolderCheck;
        console.log('', this.dataModel);
    }

    benefitnext() {
        this.dataModel['BENEFITS']['BENEFITS'] = this.secondFormGroup.value.benefits;
        console.log(this.dataModel);
    }

    Submit() {
        this.dataModel['CLAIM']['DIAGNOSIS'] = this.thirdFormGroup.value.diagnosis;
        this.dataModel['CLAIM']['PROCEDURE'] = this.thirdFormGroup.value.procedure;
        this.dataModel['CLAIM']['CONSULTAIONSTARTDATE'] = this.thirdFormGroup.value.consultationFromDate.getTime();
        this.dataModel['CLAIM']['CONSULTAIONENDDATE'] = this.thirdFormGroup.value.consultationToDate.getTime();
        this.dataModel['CLAIM']['DOCTOR'] = this.thirdFormGroup.value.doctor;
        this.dataModel['CLAIM']['SYMPTOM'] = this.thirdFormGroup.value.symptom;
        this.dataModel['CLAIM']['SYMPTOMDATE'] = this.thirdFormGroup.value.dateOfSymptom;
        this.dataModel['CLAIM']['TREATMENT'] = this.thirdFormGroup.value.treatment;
        this.dataModel['CLAIM']['OTHERINSURANCE'] = this.thirdFormGroup.value.otherInsurance;
        this.dataModel['CLAIM']['SETTLED'] = this.thirdFormGroup.value.settled;
        for (let i = 0; i < this.thirdFormGroup.value.treatment.length; i++) {
            this.dataModel['CLAIM']['TREATMENT'][i]['item'] = this.thirdFormGroup.value.attributes[i]['item'];
            this.dataModel['CLAIM']['TREATMENT'][i]['procedure'] = this.thirdFormGroup.value.attributes[i]['procedure'];
            this.dataModel['CLAIM']['TREATMENT'][i]['amount'] = this.thirdFormGroup.value.attributes[i]['amount']
        }


        console.log(this.dataModel);
    }

    attributesArray() {
        this.intializeTreatment()
    }


    handleFileChange(event, documentName) {
        this.documentArr = this.document.handleFileChange(event, documentName);
    }
    public removeDocument(index) {
        this.documentArr = this.document.removeDocument(index);
    }

    viewClaims() {
        // return this.router.navigate(['/claims']);
        return this.viewClaim = true;
    }

}
