/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE SELECTOR TEMPLATE_URL AND CLASS NAME*/
import { Component, OnInit } from '@angular/core'
import { NBaseComponent } from '../../../../../app/baseClasses/nBase.component';
import { Router, ActivatedRoute } from '@angular/router';
import { documentserviceService } from '../../services/documentservice/documentservice.service';


@Component({
    selector: 'bh-voucherdetails',
    templateUrl: './voucherdetails.template.html'
})

export class voucherdetailsComponent extends NBaseComponent implements OnInit {

    constructor(private router: Router,
        private activatedRoute: ActivatedRoute,
        private document: documentserviceService) {
        super();
    }

    data;
    documentName;
    documentArr=[];

    ngOnInit() {

        this.documentArr = this.document.getDocumentArr();
     }
    handleFileChange(event, documentName) {
        this.documentArr = this.document.handleFileChange(event, documentName);
    }
    public removeDocument(index) {
        this.documentArr =this.document.removeDocument(index);
    }
}
