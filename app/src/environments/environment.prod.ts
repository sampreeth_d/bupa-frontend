export const environment = {
    "name": "prod",
    "properties": {
        "production": true,
        "ssdURL": "http://localhost:8081/api/",
        "tenantName": "neutrinos-delivery",
        "appName": "bupa-frontend",
        "namespace": "com.neutrinos-delivery.bupa-frontend",
        "googleMapKey": "AIzaSyCSTnVwijjv0CFRA4MEeS-H6PAQc87LEoU",
        "useDefaultExceptionUI": true,
        "isIDSEnabled": "false",
        "webAppMountpoint": "web",
        "SSD_URL": "http://localhost:8081/api/"
    }
}