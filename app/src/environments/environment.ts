export const environment = {
    "name": "dev",
    "properties": {
        "production": false,
        "ssdURL": "http://localhost:8081/api/",
        "tenantName": "neutrinos-delivery",
        "appName": "bupa-frontend",
        "namespace": "com.neutrinos-delivery.bupa-frontend",
        "googleMapKey": "AIzaSyCSTnVwijjv0CFRA4MEeS-H6PAQc87LEoU",
        "useDefaultExceptionUI": true,
        "isIDSEnabled": "true",
        "webAppMountpoint": "web",
        "SSD_URL": "http://localhost:8081/api/"
    }
}